package trax.cli;

import org.apache.commons.cli.CommandLine;
import trax.plugin.ChromocenterParameters;
import trax.process.ChromocenterCalling;

public class CLIRunAction {
    /**
     * Command line
     */
    private CommandLine m_cmd;

    public CLIRunAction(CommandLine cmd)throws Exception{
        this.m_cmd=cmd;
        runSegCC();
    }


    private void runSegCC()throws Exception {
        ChromocenterParameters chromocenterParameters = new ChromocenterParameters(
                this.m_cmd.getOptionValue("iRaw"),
                this.m_cmd.getOptionValue("iSeg"),
                this.m_cmd.getOptionValue("o"));
        if (this.m_cmd.hasOption("isG")) chromocenterParameters._gaussianOnRaw = true;
        if (this.m_cmd.hasOption("isF")) chromocenterParameters._sizeFilterConnectedComponent = true;
        if (this.m_cmd.hasOption("noC")) chromocenterParameters._noChange = true;
        if (this.m_cmd.hasOption("gX"))
            chromocenterParameters._gaussianBlurXsigma =  Double.parseDouble(m_cmd.getOptionValue("gX"));

        if (this.m_cmd.hasOption("gY"))
            chromocenterParameters._gaussianBlurYsigma =  Double.parseDouble(m_cmd.getOptionValue("gY"));

        if (this.m_cmd.hasOption("gZ"))
            chromocenterParameters._gaussianBlurZsigma =  Double.parseDouble(m_cmd.getOptionValue("gZ"));

        if (this.m_cmd.hasOption("min"))
            chromocenterParameters._minSizeConnectedComponent =  Double.parseDouble(m_cmd.getOptionValue("min"));
        if (this.m_cmd.hasOption("max"))

            chromocenterParameters._maxSizeConnectedComponent =  Double.parseDouble(m_cmd.getOptionValue("max"));
        if (this.m_cmd.hasOption("f"))
            chromocenterParameters._factor=  Double.parseDouble(m_cmd.getOptionValue("f"));
        if (this.m_cmd.hasOption("n"))
            chromocenterParameters._neigh=  Integer.parseInt(m_cmd.getOptionValue("n"));

        ChromocenterCalling ccCalling= new ChromocenterCalling(chromocenterParameters);
        try {

            System.out.println("-iRaw "+chromocenterParameters.m_inputFolder+" -iSeg "+chromocenterParameters._segInputFolder+" - "+chromocenterParameters.m_outputFolder);
            ccCalling.runSeveralImages2();
        } catch (Exception e) { e.printStackTrace(); }
        System.out.println("End !!! Results available:"+ chromocenterParameters.m_outputFolder);


    }


}
