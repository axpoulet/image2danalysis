package trax.mains;

import trax.cli.CLIHelper;
import trax.cli.CLIOptionProcessed;
import trax.cli.CLIRunAction;
import trax.gui.GuiAnalysis;
import trax.plugin.ChromocenterParameters;
import trax.process.ChromocenterCalling;

import javax.swing.*;
import java.util.Arrays;
import java.util.List;
import java.time.LocalTime;

/**
 *
 */
public class main2DAnalysis {
	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		if (args.length > 0) {
			List<String> listArgs = Arrays.asList(args);
			System.setProperty("java.awt.headless", "false");

			if (listArgs.contains("-h") || listArgs.contains("-help")){
				CLIHelper.CmdHelp();
			}else{
				CLIOptionProcessed cliOption = new CLIOptionProcessed(args);
				CLIRunAction cliRunAction = new CLIRunAction(cliOption.getCommandLine());
			}
		} else {
			GuiAnalysis gui = new GuiAnalysis();
			while (gui.isShowing()) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (gui.isStart()) {
				LocalTime start = LocalTime.now();
				ChromocenterParameters CCAnalyseParameters = new ChromocenterParameters (
						gui.getInputRaw(), gui.getInputSeg(),
						gui.getOutputDir(), gui.getGaussianX(),
						gui.getGaussianY(), gui.getGaussianZ(),
						gui.getFactor(),gui.getNeigh(),
						gui.isGaussian(), gui.isFilter(),
						gui.getMax(), gui.getMin());

				ChromocenterCalling CCAnalyse = new ChromocenterCalling(CCAnalyseParameters,true);
				try {
					CCAnalyse.runSeveralImages2();
				} catch (Exception e) {
					e.printStackTrace();
				}
				JOptionPane.showMessageDialog(
						null, "Result in " + gui.getOutputDir()+" start "+start+" end "+LocalTime.now(),
						"End of the processing ", JOptionPane.INFORMATION_MESSAGE
				);
			}


		}
	}
}
