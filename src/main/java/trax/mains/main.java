package trax.mains;

import loci.common.DebugTools;
import trax.plugin.ChromocenterParameters;
import trax.process.ChromocenterCalling;

public class main {
    public static void main(String[] args) throws Exception {
/**
        DebugTools.enableLogging("OFF");
        ImagePlus [] _raw=   BF.openImagePlus("/media/titus/DATA/NucleusJ2.0/NJ2_PAPER/AUTOCROP/CROP/nuclei/2018051_1527150723.936_Ath_Col0--KAKU4-wt--CRWN1-wt--CRWN4-wt_Cot_J13_STD_FIXE_H258_R2_PART_0_C0.tif");

        System.out.println(_raw[0].getStackSize());

        _raw=   BF.openImagePlus("/media/titus/DATA/NucleusJ2.0/NJ2_PAPER/AUTOCROP/CROP/zprojection/2018051_1527150723.936_Ath_Col0--KAKU4-wt--CRWN1-wt--CRWN4-wt_Cot_J13_STD_FIXE_H258_R2_PART_Zprojection.tif");

        System.out.println("euuuu "+_raw[0].getStackSize());
 */
        String pathNucleiInput = "/home/plop/Desktop/Ellipsoid_evaluation/MASK_ELLIPSOID_CROP_FINAL";
        String pathCcInput = "/home/plop/Desktop/Ellipsoid_evaluation/CC_ELLISPOID";
        String pathMerged = "/home/plop/Desktop/Ellipsoid_evaluation/Merged";
        DebugTools.enableLogging("OFF");
        ChromocenterParameters CCAnalyseParameters =new  ChromocenterParameters(
                "/home/plop/Desktop/Ellipsoid_evaluation/Merged",
                "/home/plop/Desktop/Ellipsoid_evaluation/MASK_ELLIPSOID_CROP_FINAL",
                "/home/plop/Desktop/Ellipsoid_evaluation/CC_ELLISPOID",true,1,0.3);
        ChromocenterCalling CCAnalyse= new ChromocenterCalling(CCAnalyseParameters);
        CCAnalyse.just3D();
        /**
        DebugTools.enableLogging("OFF");
        List<String> listArgs = Arrays.asList(args);
        System.setProperty("java.awt.headless", "false");


        if(listArgs.contains("-h") ||listArgs.contains("-help")){
            CLIHelper command = new CLIHelper(args );
        }

        else{
            CLIActionOptionCmdLine command = new CLIActionOptionCmdLine( args);
            CLIRunAction runCmd =new CLIRunAction(command.getCmd());
        }
         */
    }
}
